<?php
	class Animal
	{
		public $name;
	  	public $legs;
	  	public $cold_blooded;

		function __construct($name, $legs) 
		{
		    $this->name = $name; 
		    $this->legs = $legs;
		}

		function get_name() 
		{
		    return $this->name;
		}

		function get_legs()
		{
			return $this->legs;
		}

		function set_cold_blooded($cold_blooded)
		{
			$this->cold_blooded = $cold_blooded;
		}

		function get_cold_blooded()
		{
			return $this->cold_blooded;
		}
	}

?>