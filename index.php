<?php
	require_once 'animal.php';
	require_once 'ape.php';
	require_once 'frog.php';

	$sheep = new Animal("shaun", 2);
	echo $sheep->get_name(); // "shaun"
	echo "<br>";
	echo $sheep->get_legs(); // 2
	echo "<br>";
	$sheep->set_cold_blooded('false');
	echo $sheep->get_cold_blooded(); // false
	echo "<br>";
	echo "<br>";

	$sungokong = new Ape("kera sakti", 2);
	echo $sungokong->get_name(); // "kera sakti"
	echo "<br>";
	echo $sungokong->get_legs(); // 2
	echo "<br>";
	$sungokong->yell(); // "Auooo"
	echo "<br>";
	echo "<br>";

	$kodok = new Frog("buduk", 4);
	echo $kodok->get_name(); // "buduk"
	echo "<br>";
	echo $kodok->get_legs(); // 4
	echo "<br>";
	$kodok->jump() ; // "hop hop"
?>